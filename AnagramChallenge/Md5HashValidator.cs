﻿namespace AnagramChallenge
{
    /// <summary>
    /// Calculates and validates MD5 hash against <see cref="HashValidator.HashToMatch"/>.
    /// </summary>
    internal class Md5HashValidator : HashValidator
    {
        /// <summary>
        /// Creates a new instance of MD5HashCalculator.
        /// </summary>
        /// <param name="hashToMatch">Value to match.</param>
        public Md5HashValidator(string hashToMatch) : base(hashToMatch)
        {
        }

        /// <summary>
        /// Validates MD5 hash of <see cref="input"/> against <see cref="HashValidator.HashToMatch"/>.
        /// </summary>
        /// <param name="input">String for which has needs to be calculated.</param>
        /// <returns>True if hash of <see cref="input"/> matches <see cref="HashValidator.HashToMatch"/>.</returns>
        public override bool IsMatch(string input)
        {
            return input.ComputeMD5Hash() == HashToMatch;
        }
    }
}