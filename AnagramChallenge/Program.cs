﻿using System;
using System.Diagnostics;
using System.IO;

namespace AnagramChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            string source = "poultry outwits ants";
            //string expectedHash = "e4820b45d2277f3844eac66c903e84be"; //printout stout yawls
            string expectedHash = "23170acc097c24edb98fc5488ab033fe"; //ty outlaws printouts 

            var stopWatch = Stopwatch.StartNew();
            AnagramSolver solver = new AnagramSolver(source, File.ReadAllLines("wordlist"));
            Console.WriteLine("Starting search...");
            var foundAnagram = solver.FindAnagramWithHash(new Md5HashValidator(expectedHash));

            Console.WriteLine(foundAnagram != null ? $"Solution: {foundAnagram}" : "Nothing found.");
            Console.WriteLine($"Elapsed time: {stopWatch.Elapsed}");
        }
    }
}
