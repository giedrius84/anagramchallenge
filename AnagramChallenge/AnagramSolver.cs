﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AnagramChallenge
{
    /// <summary>
    /// Solves given anagrams by verifying MD5 hash value.
    /// </summary>
    internal class AnagramSolver
    {
        private string _anagram;

        /// <summary>
        /// Anagram to solve.
        /// </summary>
        public string Anagram
        {
            get => _anagram;
            set
            {
                _anagram = value;
                AnagramWordCount = value.Count(c => c == ' ') + 1;
            }
        }

        public List<string> DictionaryWords { get; }

        public int AnagramWordCount { get; set; }

        private readonly Dictionary<string, List<string>> _subsetsForAnagrams = new Dictionary<string, List<string>>();

        /// <summary>
        /// Initializes an instance of <see cref="AnagramSolver"/>.
        /// </summary>
        /// <param name="anagram">Anagram to solve.</param>
        /// <param name="dictionaryWords">List of words to create the anagram from.</param>
        public AnagramSolver(string anagram, IEnumerable<string> dictionaryWords)
        {
            if (string.IsNullOrEmpty(anagram))
            {
                throw new ArgumentException(nameof(anagram));
            }
            Anagram = anagram;
            DictionaryWords = dictionaryWords.Select(w => w.Trim().ToLower()).Where(w => w.Length > 0).ToList();
        }

        /// <summary>
        /// Finds an anagram that matches provided hash value. Uses <see cref="DictionaryWords"/> to find the anagram.
        /// </summary>
        /// <param name="hashValidator">Verifies if found anagram matches the expected hash.</param>
        /// <returns>Anagram that <see cref="hashValidator"/> accepts as expected match, null otherwise.</returns>
        public string FindAnagramWithHash(HashValidator hashValidator)
        {
            return FindAnagram(Anagram, DictionaryWords, new List<string>(), hashValidator);
        }

        /// <summary>
        /// Recursively searches for anagram which matches provided <see cref="hashValidator"/>.
        /// </summary>
        /// <param name="anagram">Anagram to solve.</param>
        /// <param name="dictionaryWords">Dictionary to use for solving the anagram.</param>
        /// <param name="foundAnagramWords">Anagram words found so far.</param>
        /// <param name="hashValidator">Verifies if found anagram matches the expected hash.</param>
        /// <returns>Anagram that <see cref="hashValidator"/> accepts as expected match, null otherwise.</returns>
        public string FindAnagram(string anagram, List<string> dictionaryWords, List<string> foundAnagramWords, HashValidator hashValidator)
        {
            //exit condition
            if (foundAnagramWords.Count == AnagramWordCount)
            {
                string anagramToVerify = string.Join(" ", foundAnagramWords);
                if (anagramToVerify.Length != Anagram.Length) return null;

                Console.WriteLine($"Verifying anagram: {anagramToVerify}");
                return hashValidator.IsMatch(anagramToVerify) ? anagramToVerify : null;
            }

            //dictionary of words that are subsets of the given anagram
            List<string> subsetDictionary = CreateSubsets(anagram, dictionaryWords);

            foreach (string word in subsetDictionary)
            {
                foundAnagramWords.Add(word);
                string remainingAnagram = anagram.Subtract(word);

                //recursively solve remaining anagram
                string result = FindAnagram(remainingAnagram, subsetDictionary, foundAnagramWords, hashValidator);
                if (result != null)
                {
                    return result;
                }

                //backtrack by removing the word which was added
                foundAnagramWords.Remove(word);

            }

            return null;
        }

        /// <summary>
        /// Creates a list of anagram subsets.
        /// </summary>
        /// <param name="anagram">Anagram to create subsets for.</param>
        /// <param name="allWords">Words for subsets.</param>
        /// <returns>List of subsets.</returns>
        public List<string> CreateSubsets(string anagram, List<string> allWords)
        {
            //reuse subset list if it has already been created for the given anagram
            if (_subsetsForAnagrams.ContainsKey(anagram))
            {
                return _subsetsForAnagrams[anagram];
            }

            List<string> dictionary = new List<string>();
            string anagramSource = anagram.Replace(" ", string.Empty).ToLower();

            foreach (var word in allWords)
            {
                if (word.IsSubset(anagramSource))
                {
                    dictionary.Add(word);
                }
            }

            _subsetsForAnagrams.Add(anagram, dictionary);
            return dictionary;
        }
    }
}


abstract class Solver
{
    protected ICollection<string> Words { get; set; }

    protected abstract Anagram Solve(Anagram anagramToSolve);
}

internal class Anagram : IEquatable<Anagram>
{
    public IAnagramEqualityComparer Comparer { get; set; }

    public bool Equals(Anagram other)
    {
        return Comparer.AreEqual(this, other);
    }
}

internal interface IAnagramEqualityComparer
{
    bool AreEqual(Anagram anagram1, Anagram anagram2);
}

class HashComparer : IComparer<Anagram>
{
    public int Compare(Anagram x, Anagram y)
    {
        return 1;
    }
}

