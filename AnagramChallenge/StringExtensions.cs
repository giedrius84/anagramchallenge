using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AnagramChallenge
{
    static class StringExtensions
    {
        static readonly UTF8Encoding Utf8Encoding = new UTF8Encoding();
        static readonly MD5 Md5 = MD5.Create();

        public static bool IsSubset(this string subset, string set)
        {
            if (subset == null)
            {
                throw new ArgumentNullException(nameof(subset));
            }
            if (set == null)
            {
                throw new ArgumentNullException(nameof(set));
            }
            if (subset.Length > set.Length) return false;

            set = string.Concat(set.OrderBy(c => c));
            subset = string.Concat(subset.OrderBy(c => c));

            if (set.Contains(subset)) return true;

            foreach (char c in subset)
            {
                int index = set.IndexOf(c);
                if (index > -1)
                {
                    set = set.Remove(index, 1);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public static string ComputeMD5Hash(this string input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            byte[] bytes = Utf8Encoding.GetBytes(input);
            var hash = Md5.ComputeHash(bytes);

            var sb = new StringBuilder();
            foreach (var h in hash)
            {
                sb.Append(h.ToString("X2"));
            }

            return sb.ToString().ToLower();
        }

        public static string Subtract(this string set, string subset)
        {
            if (subset == null)
            {
                throw new ArgumentNullException(nameof(subset));
            }
            if (set == null)
            {
                throw new ArgumentNullException(nameof(set));
            }

            string result = set;
            foreach (var ch in subset)
            {
                int index = result.IndexOf(ch);
                if (index > -1)
                {
                    result = result.Remove(result.IndexOf(ch), 1);
                }
            }

            return result;
        }
    }
}