﻿namespace AnagramChallenge
{
    /// <summary>
    /// Base class for validating hash.
    /// </summary>
    internal abstract class HashValidator
    {
        protected HashValidator(string hashToMatch)
        {
            HashToMatch = hashToMatch;
        }

        protected string HashToMatch { get; }

        /// <summary>
        /// Validates hash of <see cref="input"/> against <see cref="HashToMatch"/>.
        /// </summary>
        /// <param name="input">String for which has needs to be calculated.</param>
        /// <returns>True if hash of <see cref="input"/> matches <see cref="HashToMatch"/>.</returns>
        public abstract bool IsMatch(string input);
    }
}