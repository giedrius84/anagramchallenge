﻿using System;
using AnagramChallenge;
using NUnit.Framework;

namespace AnagramChallengeTests
{
    [TestFixture]
    public class StringExtensionsTests
    {
        [Test]
        public void IsSubsetThrowsWhenArgumentsAreNull()
        {
            Assert.Throws<ArgumentNullException>(() => ((string)null).IsSubset("test"));
            Assert.Throws<ArgumentNullException>(() => "subset".IsSubset(null));
        }

        [Test]
        [TestCase("test", "test", true)]
        [TestCase("test", "supertest", true)]
        [TestCase("test", "tteesstt", true)]
        [TestCase("", "not", true)]
        [TestCase("test", "not", false)]
        [TestCase("test", "", false)]
        [TestCase("test", "te", false)]
        public void IsSubsetFindsSubset(string subset, string set, bool isSubset)
        {
            Assert.AreEqual(isSubset, subset.IsSubset(set));
        }

        [Test]
        public void SubractThrowsWhenArugmentsAreNull()
        {
            Assert.Throws<ArgumentNullException>(() => ((string)null).Subtract("test"));
            Assert.Throws<ArgumentNullException>(() => "subset".Subtract(null));
        }

        [Test]
        [TestCase("test", "test", "")]
        [TestCase("abcdef", "bcd", "aef")]
        [TestCase("not", "", "not")]
        [TestCase("test", "no", "test")]
        [TestCase("", "test", "")]
        [TestCase("", "", "")]
        public void SubtractRemovesCharacters(string set, string subset, string result)
        {
            Assert.AreEqual(result, set.Subtract(subset));
        }
    }
}
