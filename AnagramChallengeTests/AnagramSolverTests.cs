﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using AnagramChallenge;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace AnagramChallengeTests
{
    [TestFixture]
    public class AnagramSolverTests
    {
        [Test]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void ConstructorThrowsIfAnagramArgumentIsNull()
        {
            Assert.Throws<ArgumentException>(() => new AnagramSolver(null, Enumerable.Empty<string>()));
            Assert.Throws<ArgumentException>(() => new AnagramSolver(string.Empty, Enumerable.Empty<string>()));
        }

        [Test]
        [TestCase("test", 1)]
        [TestCase("test1 test2", 2)]
        public void ConstructorSetsWordCountProperty(string anagram, int expectedWordCount)
        {
            AnagramSolver solver = new AnagramSolver(anagram, Enumerable.Empty<string>());
            Assert.AreEqual(anagram, solver.Anagram);
            Assert.AreEqual(expectedWordCount, solver.AnagramWordCount);
            Assert.AreEqual(Enumerable.Empty<string>(), solver.DictionaryWords);
        }

        [Test]
        public void DictionaryDoesNotContainEmptyWords()
        {
            string dictWord = "test";
            List<string> testDictionary = new List<string>
            {
                string.Empty,
                " ",
                dictWord
            };
            AnagramSolver solver = new AnagramSolver("testAnagram", testDictionary);
            Assert.AreEqual(1, solver.DictionaryWords.Count);
            Assert.AreEqual(dictWord, solver.DictionaryWords[0]);
        }

        [Test]
        public void CreateSubsetsReturnAllPermutations()
        {
            AnagramSolver solver = new AnagramSolver("testSolver", Enumerable.Empty<string>());
            List<string> testWords = new List<string>
            {
                "abc",
                "tset",
                "ts",
                "est"
            };
            var subsets = solver.CreateSubsets("te st", testWords);
            Assert.AreEqual(3, subsets.Count);
            Assert.AreEqual(new[] { "tset", "ts", "est" }, subsets);
        }

        [Test]
        public void FindAnagramUsesHashValidator()
        {
            List<string> testDict = new List<string>
            {
                "abc",
                "tset",
                "ts",
                "est"
            };
            AnagramSolver solver = new AnagramSolver("test", testDict);
            string foundAnagram = solver.FindAnagramWithHash(new StringEqualityComparer("tset"));
            Assert.AreEqual(foundAnagram, "tset");
        }

        [Test]
        public void FindAnagramWithNoAnagramFoundReturnsNull()
        {
            List<string> testDict = new List<string>
            {
                "abc",
                "def"
            };
            AnagramSolver solver = new AnagramSolver("test", testDict);
            string foundAnagram = solver.FindAnagramWithHash(new NoHashValidator(() => true));
            Assert.IsNull(foundAnagram);
        }

        [Test]
        public void FindAnagramFindsAllAnagrams()
        {
            List<string> testDict = new List<string>
            {
                "abc",
                "tset",
                "def",
                "etts",
                "321"
            };
            AnagramSolver solver = new AnagramSolver("test 123", testDict);
            var validator = new AccumulatingValidator("");
            string foundAnagram = solver.FindAnagramWithHash(validator);
            Assert.IsNull(foundAnagram);
            CollectionAssert.AreEquivalent(new [] { "tset 321", "321 tset", "etts 321", "321 etts" }, validator.Anagrams);
        }
    }

    #region Hash validators
    class NoHashValidator : HashValidator
    {
        public NoHashValidator(Func<bool> isMatch) : base(string.Empty)
        {
            IsMatchFunc = isMatch;
        }

        public Func<bool> IsMatchFunc { get; set; }

        public override bool IsMatch(string input)
        {
            return IsMatchFunc.Invoke();
        }
    }

    class StringEqualityComparer : HashValidator
    {
        public StringEqualityComparer(string hashToMatch) : base(hashToMatch)
        {
        }

        public override bool IsMatch(string input)
        {
            return input == HashToMatch;
        }
    }

    class AccumulatingValidator : HashValidator
    {
        public AccumulatingValidator(string hashToMatch) : base(hashToMatch)
        {
            Anagrams = new List<string>();
        }

        public override bool IsMatch(string input)
        {
            Anagrams.Add(input);
            return false;
        }

        public List<string> Anagrams { get; set; }
    }
    #endregion
}
